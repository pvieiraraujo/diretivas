import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diretiva-ngif',
  templateUrl: './diretiva-ngif.component.html',
  styleUrls: ['./diretiva-ngif.component.css']
})
export class DiretivaNgifComponent implements OnInit {

  cursos: string[] = ['Angular2', 'Java', 'Bootstrap'];

  mostrarCursos: boolean = true;

  dataHoje = '2020-09-12';

  constructor() { }

  ngOnInit(): void {
  }

  onMostrarCursos(){
    this.mostrarCursos = this.alternarVisualizacao();
  }

  alternarVisualizacao(): boolean{
  return !this.mostrarCursos
  }

}
