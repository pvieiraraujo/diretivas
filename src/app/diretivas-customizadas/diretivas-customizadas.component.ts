import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diretivas-customizadas',
  templateUrl: './diretivas-customizadas.component.html',
  styleUrls: ['./diretivas-customizadas.component.css']
})
export class DiretivasCustomizadasComponent implements OnInit {

  mostrarCursos: boolean = true;
  cursos: string[] = ['Angular2', 'Java', 'Bootstrap'];

  constructor() { }

  ngOnInit(): void {
  }

  onMostrarCursos(){
    this.mostrarCursos = this.alternarVisualizacao();
  }

  alternarVisualizacao(): boolean{
    return !this.mostrarCursos
    }

}
