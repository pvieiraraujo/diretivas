import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExmploNgcontentComponent } from './exmplo-ngcontent.component';

describe('ExmploNgcontentComponent', () => {
  let component: ExmploNgcontentComponent;
  let fixture: ComponentFixture<ExmploNgcontentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExmploNgcontentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExmploNgcontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
