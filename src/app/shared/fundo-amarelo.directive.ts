import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: 'p[fundoAmarelo]'
})
export class fundoAmareloDirective {

  constructor(private _elementRef: ElementRef,
    private _Renderer2: Renderer2
  ) {
    console.log(this._elementRef);
    this._elementRef.nativeElement.style.backgroundColor = 'yellow'
    this._Renderer2.setStyle(this._elementRef.nativeElement,
      'background-color',
      'yellow'
    );
  }

}
