import { Directive, HostListener, ElementRef, Renderer2, HostBinding} from '@angular/core';

@Directive({
  selector: '[highlightMouse]'
})
export class highlightMouseDirective {

  @HostListener('mouseenter') onMouseOver(){
  //   this._renderer.setStyle(
  //       this._elementRef.nativeElement,
  //       'background', 'yellow'
  //   )
    this.backgroundColor = 'yellow';
   }

  @HostListener('mouseenter') onMouseLeave(){
  //   this._renderer.setStyle(
  //       this._elementRef.nativeElement,
  //       'background', 'white'
  //   )
    this.backgroundColor = 'white';
   }

  @HostBinding('style.backgroundColor') backgroundColor: String;

   constructor(
  //  private _elementRef: ElementRef,
  // private _renderer: Renderer2  
   ) { }

}
